# Welcome to Adélie Linux
# /etc/bash/bashrc - run by bash on every startup

# Prior to 1.0a3, we did a lot of heavy lifting here.
# Massive cleanup for 1.0a3 makes things faster and more efficient.

# No interactivity = no point
if [[ $- != *i* ]]; then
	return
fi

# Extra stat(3) call to invalidate hash cache.  Better UX, but lots of waste.
# shopt -s checkhash

# Handle window resizing after a curses program quits
shopt -s checkwinsize

# Append instead of overwriting histfile.
shopt -s histappend

# Allow users to correct typos in history substs.
shopt -s histreedit

# Save commands as they were typed.
shopt -s lithist

# Don't show every command available if a user types \t on a bare line
shopt -s no_empty_cmd_completion

# Expand things like \n \r \t when using built-in echo(1).
shopt -s xpg_echo


do_colour() {
	local colourise=false
	LS_COLORS=
	eval "$(dircolors -b)"
	if [[ -n ${LS_COLORS:+set} ]]; then
		colourise=true
	else
		unset LS_COLORS
	fi

	if ${colourise} ; then
		if [[ ${EUID} == 0 ]] ; then
			PS1='\[\e[01m\]\h\[\e[22m\] \[\e[01;36m\]\w\[\e[00m\] \[\e[01;31m\]\$\[\e[00m\] '
		else
			PS1='\u on \[\e[01m\]\h\[\e[22m\] \w \[\e[01;32m\$\[\e[00m\] '
		fi

		alias ls='ls --color=auto'
		alias grep='grep --colour=auto'
		alias egrep='egrep --colour=auto'
		alias fgrep='fgrep --colour=auto'
	else
		if [[ ${EUID} == 0 ]] ; then
			# show root@ when we don't have colors
			PS1='! \u on \h \w \$ '
		else
			PS1='\u on \h \w \$ '
		fi
	fi
}

do_colour

for sh in /etc/bash/bashrc.d/* ; do
	[[ -r ${sh} ]] && source "${sh}"
done

unset do_colour
