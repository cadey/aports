# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=hexchat
pkgver=2.12.4
pkgrel=1
pkgdesc="A popular and easy to use graphical IRC (chat) client"
url="https://hexchat.github.io"
arch="all"
license="GPL2+"
depends=""
makedepends="gtk+2.0-dev openssl-dev dbus-glib-dev perl-dev libsexy-dev
	libnotify-dev libproxy-dev bash libtool autoconf automake"
install=""
subpackages="$pkgname-doc $pkgname-lang $pkgname-perl:_perl"
source="https://dl.hexchat.net/hexchat/hexchat-$pkgver-repack.tar.xz
	pixdata.patch
	"

builddir="$srcdir"/hexchat-$pkgver
prepare() {
	cd "$builddir"
	default_prepare
	autoreconf -vif
}

build() {
	cd "$builddir"
	LUA=lua5.3 \
	./configure --prefix=/usr \
		--sysconfdir=/etc \
		--enable-openssl \
		--enable-dbus \
		--disable-textfe \
		--enable-perl \
		--disable-python \
		--disable-lua
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
	# not worth a -dev pkg
	rm -r "$pkgdir"/usr/include
}

_perl() {
	pkgdesc="Perl plugin for Hexchat"
	depends="$pkgname=$pkgver-r$pkgrel"
	install_if="$pkgname=$pkgver-r$pkgrel perl"
	mkdir -p "$subpkgdir"/usr/lib/hexchat/plugins
	mv "$pkgdir"/usr/lib/hexchat/plugins/perl.so \
		"$subpkgdir"/usr/lib/hexchat/plugins
}

sha512sums="30d42f5b488abec3fa457254720a39f62619338a5a2c3fe2e5a255aafe1b19817451b01cd260eab90868df1ebf9f663c60b78b6db974ca3c777272327c0b8a25  hexchat-2.12.4-repack.tar.xz
5cb7ac95e6d53d677d7ec82485636f2c36003ba7fa0c4d4d353095dc6207c51abdc7a2230d43616895fef8ce2c7c2096bec21ac47117d0adbc7416ff3d4ba2c3  pixdata.patch"
