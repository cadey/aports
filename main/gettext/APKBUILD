# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Maintainer: Carlo Landmeter <clandmeter@gmail.com>
pkgname=gettext
pkgver=0.19.8.1
pkgrel=1
pkgdesc="GNU locale utilities"
url="http://www.gnu.org/software/gettext/gettext.html"
arch="all"
license="GPL"
depends=
# do _not_ add the optional dependencies on libcroco or glib
# they depend on gettext and would introduce cyclic dependencies
makedepends="perl ncurses-dev libxml2-dev"
source="ftp://ftp.gnu.org/gnu/$pkgname/$pkgname-$pkgver.tar.xz
	localename-fix.patch
	skip-tests-musl.patch
	"
subpackages="$pkgname-doc $pkgname-static $pkgname-dev $pkgname-lang libintl $pkgname-asprintf $pkgname-libs"

build() {
	cd "$builddir"
	# force using system posix complaint printf
	# the test is broken and fails with ash
	gt_cv_func_printf_posix=yes \
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--enable-threads=posix \
		--disable-java \
		--enable-static
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make -j1 DESTDIR="$pkgdir" install
	rm "$pkgdir"/usr/lib/charset.alias
}

static() {
	pkgdesc="libintl static libraries"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/*.a "$subpkgdir"/usr/lib/
}

libintl() {
	pkgdesc="GNU gettext runtime library"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libintl.so.* "$subpkgdir"/usr/lib
	chmod +x "$subpkgdir"/usr/lib/libintl.so.*
}

asprintf() {
	pkgdesc="GNU gettext asprintf library"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libasprintf.so.* "$subpkgdir"/usr/lib
}

sha512sums="3553227b62f2a7d9b67c881ef889c030a6a21d5ecd210c4bf3d649df0b37193a99a68cf8fd5f2c69b6a87e847035dd9576f9bcb9363422866e26b04f4f6dd431  gettext-0.19.8.1.tar.xz
2765f8d9d72d85ad0adb87ee0edd83d3aec59995ef21a3c8bbd1ac20a3680058a2122bd3f6c37be3f50cb5ea00c19b3ad569a47ceedc8ae2cb4a6e8d4e30976d  localename-fix.patch
8fced6ec5c1f54686545aa91759f8501a0ccfa4bb66a781d282e65f7309c70ab74bf753f4969374facb135b7b9341e26d8a3e27679ea09cb1543b82dfb8a16dd  skip-tests-musl.patch"
