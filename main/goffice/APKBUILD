# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=goffice
pkgver=0.10.35
pkgrel=1
_maj=${pkgver%%.*}
_min=${pkgver#${_maj}.}
_min=${_min%%.*}
pkgdesc="A library of document-centric objects and utilities built on top of GLib and Gtk+"
url="http://www.gnome.org"
arch="all"
license="GPL"
makedepends="librsvg-dev intltool gtk+-dev autoconf automake libtool gtk-doc
	libxslt-dev gobject-introspection-dev glib-dev libgsf-dev cairo-dev
	libxml2-dev gtk+3.0-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.gnome.org/sources/$pkgname/$_maj.$_min/$pkgname-$pkgver.tar.xz"

prepare() {
	default_prepare
	libtoolize --force && aclocal && autoconf \
		&& automake --add-missing --gnu
}

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--localstatedir=/var \
		--disable-static
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="68a274e73b8c237966ff300f129362d5a7a861e0281ca0dca9a0850f0ea30644f16de0eb5480ac8a399e7ff40699af5ddfcd79593c42f06d296ab3e778c368d2  goffice-0.10.35.tar.xz"
