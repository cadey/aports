# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=attr
pkgver=2.4.47
pkgrel=7
pkgdesc="Utilities for managing filesystem extended attributes"
url="https://savannah.nongnu.org/projects/attr"
arch="all"
license="GPL2+ LGPL2+"
depends=""
options="!checkroot"
makedepends="libtool autoconf automake bash gzip"
checkdepends="perl"
subpackages="$pkgname-dev $pkgname-doc libattr"
source="http://download.savannah.nongnu.org/releases/attr/attr-$pkgver.src.tar.gz
	fix-decls.patch
	fix-throw.patch
	test-runner-musl.patch
	test-runner-perl.patch
	"

prepare() {
	cd "$builddir"
	default_prepare
	update_config_sub

	sed -i -e '/HAVE_ZIPPED_MANPAGES/s:=.*:=false:' \
		include/builddefs.in
}

build() {
	cd "$builddir"

	OPTIMIZER="${CFLAGS}" DEBUG=-DNDEBUG INSTALL_USER=root INSTALL_GROUP=root ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/ \
		--exec-prefix=/ \
		--sbindir=/bin \
		--bindir=/usr/bin \
		--libdir=/lib \
		--libexecdir=/usr/lib \
		--includedir=/usr/include \
		--mandir=/usr/share/man \
		--datadir=/usr/share \
		--disable-gettext
	make
}

check() {
	cd "$builddir"
	make tests
}

package() {
	cd "$builddir"
	make -j1 DESTDIR="$pkgdir" \
		install install-lib install-dev

	# provided by man-pages
	rm -r "$pkgdir"/usr/share/man/man2 \
		"$pkgdir"/usr/share/man/man5/attr.5
}

libattr() {
	pkgdesc="Dynamic library for extended attribute support"
	replaces="attr"
	mkdir -p "$subpkgdir"/lib
	mv "$pkgdir"/lib/lib*.so.* "$subpkgdir"/lib/
}

sha512sums="2a333f63655758298650cf8f89c175efdc0112dcc4013e8d023e2a9a69f177e4bcb1f1d10b6666d6f2027dca4dec0833d54749952ab153d28367e1a72e6e2831  attr-2.4.47.src.tar.gz
fa7925f63c611e39b28adbf8fa3838bd91c245e4694957c1a8e212cb8f289eb62e04f50806227f6c1947d432ddf7633a471c13dd08d513d1e2b8a9ac1906cb33  fix-decls.patch
d758b864bac9bdbc3360df2e7a3bc7d04e06789975cf539b8e2b98b1d874744b55c80f0502e283f7233d6ec41f8a39624fe07b512a7fdc6af8d19dd3af5f9f5a  fix-throw.patch
da4b903ae0ba1c72bae60405745c1135d1c3c1cefd7525fca296f8dc7dac1e60e48eeba0ba80fddb035b24b847b00c5a9926d0d586c5d7989d0428e458d977d3  test-runner-musl.patch
d10821cc73751171c6b9cc4172cf4c85be9b6e154782090a262a16fd69172a291c5d5c94587aebcf5b5d1e02c27769245d88f0aa86478193cf1a277ac7f4f18e  test-runner-perl.patch"
